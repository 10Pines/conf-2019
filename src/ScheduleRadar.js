import {redRoom, orangeRoom, yellowRoom, greenRoom, northRoom, capacitationRoom, openspacitoRoom, cocinaRoom} from './Rooms';

const buildTalk = (title, author, room, details, feedbackLink, meetLink) => {
  return {
    title,
    author,
    room,
    details,
    feedbackLink,
    meetLink,
  }
};

const schedule = [
  {
    time: '15.00 PM',
    description: 'Introducción al radar 🔗',
    break: true,
    link: 'https://meet.google.com/wnx-gnwn-ech',
  },
  {
    time: '15.20 PM',
    talks: [
      buildTalk('Buenos Sueldos - Cocina', 'NicoR, Maite, LucasW, MatiM, David, Jorge', cocinaRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Buenos Sueldos - Sala Amarilla', 'Diega, NicoV, Joe, Luciana, Charlie, Marta', yellowRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Buenos Sueldos - Sala Norte', 'Aye, Diego, Fedex, SantiN, Coco, Feche', northRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Buenos Sueldos - Sala Roja', 'Belu, Dario, Nicky, Jess, NahueV, Tom, Mica', redRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Crecimiento profesional y aprendizaje continuo - Capacitaciones', 'Cande, Emi, Leo, Daro, Tomas, Juli Rossi, Mo', capacitationRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Crecimiento profesional y aprendizaje continuo - Openspacito', 'Dani, Gasti, Gise, Tizzi, Eze, Ivo, SantiP', openspacitoRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Crecimiento profesional y aprendizaje continuo - Sala Naranja', 'Lowy, MatyF, Juli Romero, Bianca, Joa, Ludat, Rick', orangeRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Crecimiento profesional y aprendizaje continuo - Sala Verde', 'Lucho, Fer, Gaby, Maru, Debi, Lauta, Maiu', greenRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
    ]
  },
  {
    time: '16.00 PM',
    description: 'Break',
    break: true
  },
  {
    time: '16.05 PM',
    talks: [
      buildTalk('Calidad humana - Capacitaciones', 'Lowy, Joe, Luciana, NicoR, Eze, Mo, Daro', capacitationRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Calidad humana - Openspacito', 'Aye, Gasty, Cande, Nicky, Santi, Jorge, Ivo', openspacitoRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Calidad humana - Sala Naranja', 'Gise, Diego, NicoV, Bianca, Lauta, Mica', yellowRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Calidad humana - Sala Verde', 'Fer, Dani, Juli Romero, Tom, Charlie, Maite, Maiu', greenRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Gestion Participativa - Cocina', 'Emi, Belu, MatiM, David, Fedex, Tomas', cocinaRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Gestion Participativa - Sala Amarilla', 'MatiF, Leo, Lucho, Juli, Ludat, Joa', orangeRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Gestion Participativa - Sala Norte', 'Diega, Feche, Maru, Jess, Debi, SantiN, NahueV', northRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Gestion Participativa - Sala Roja', 'Dario, Gaby, LucasW, Tizzi, Coco, Marta, Rick', redRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
    ]
  },
  {
    time: '16.45 PM',
    description: 'Break',
    break: true
  },
  {
    time: '16.50 PM',
    talks: [
      buildTalk('Calidad tecnica - Capacitaciones', 'Dario, NicoR, Diega, Bian, Maiu, NahueV, David', capacitationRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Calidad tecnica - Openspacito', 'Fer, Gasty, Leo, Maru, Ivo, Ludat, SantiN', openspacitoRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Calidad tecnica - Sala Naranja', 'MatiM, Lowy, LucasW, Mai, SantiP, Juli Rossi, Joa', orangeRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Calidad tecnica - Sala Verde', 'Emi, Joe, Tom, Mica, Daro, Debi, Rick', greenRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Proyectos interesantes - Cocina', 'Gise, Cande, Luciana, MatyF, Charlie, Fedex', cocinaRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Proyectos interesantes - Sala Amarilla', 'Dani, Nicky, Gaby, Feche, Lauta, Coco, Tomas', yellowRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Proyectos interesantes - Sala Norte', 'Aye, NicoV, Tizzi, Lucho, Eze, Mo', northRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
      buildTalk('Proyectos interesantes - Sala Roja', 'Diego, Juli Romero, Belu, Jorge, Jess, Marta', redRoom, '', 'https://miro.com/welcomeonboard/L29CSHMrUzRXa1d6aFJUYXpuNGRsWXZMTjkrK0hoTDBUYUczK0pOZTZ3NFlGakNmT3pXdUd5NHJhd2FQL2pXY21jWlkvWXlNeGZPK3BBOER6L05IVTAyNm1uU3M2aUsraEZHZnFuczRBQTdUYS9NMmRBQ2ZIQ1YzVHlyNlBYZmhnbHpza3F6REdEcmNpNEFOMmJXWXBBPT0hdjE=?share_link_id=517926385231'),
    ]
  },
  {
    time: '17.30 PM',
    description: 'Break',
    break: true
  },
  {
    time: '17.35 PM',
    description: 'Cierre 🔗',
    break: true,
    link: 'https://meet.google.com/wnx-gnwn-ech',
  },
];

export default schedule;
