# 10PinesConf Schedule

## Requirements
This project uses node version `16.14.0`, please run `nvm use` or check your version. 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

### `npm test`

### `npm run build`

### `npm run eject`

## Deployment

The project is hosted using gitlab pages.
- Making a PR and merging should trigger the deploy.
- Browse `cronograma-conf.10pines.com`
- Profit